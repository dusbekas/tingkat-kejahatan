package com.happy.group.bogorcrimelevel.activity;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.happy.group.bogorcrimelevel.DrawOnMap;
import com.happy.group.bogorcrimelevel.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SafeRouteActivity extends AppCompatActivity implements OnMapReadyCallback {

    protected static String TAG = SafeRouteActivity.class.getName();

    private GoogleMap mMap;
    private String points, name;
    private List<LatLng> arraylatLng = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safe_route);

        if (savedInstanceState==null) {
            Bundle extras = getIntent().getExtras();
            if(extras==null) {
                points = "";
                name = "";
            } else {
                points = extras.getString("POINTS");
                name = extras.getString("NAME");
                Log.d(TAG, "POINTS: " + points);
            }
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(name);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Getting way points.
        try {
            JSONArray jsonArray = new JSONArray(points);
            for (int i=0; i<jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                double lat = jsonObject.getDouble("lat");
                double lng = jsonObject.getDouble("lng");
                LatLng latLng = new LatLng(lat, lng);
                arraylatLng.add(latLng);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
        final DrawOnMap drawOnMap = new DrawOnMap();
        drawOnMap.draw(mMap);
        drawOnMap.setUpGoogleMapScreen(mMap);
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                drawOnMap.positionCamera(arraylatLng.get(0), arraylatLng.get(arraylatLng.size()-1), mMap);
                googleMap.addPolyline(new PolylineOptions().addAll(arraylatLng));
                googleMap.addMarker(new MarkerOptions().position(arraylatLng.get(0)));
                googleMap.addMarker(new MarkerOptions().position(arraylatLng.get(arraylatLng.size()-1)));
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
