package com.happy.group.bogorcrimelevel.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by arman on 22/12/2017.
 */

public class Route {
    private String name;
    private String points;

    public Route (){

    }

    public Route(String name) {
        this.name = name;
    }

    public Route(String name, String points) {
        this.name = name;
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
