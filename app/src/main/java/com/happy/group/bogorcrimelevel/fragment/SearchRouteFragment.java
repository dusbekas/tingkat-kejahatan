package com.happy.group.bogorcrimelevel.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.TravelMode;
import com.happy.group.bogorcrimelevel.DrawOnMap;
import com.happy.group.bogorcrimelevel.R;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchRouteFragment extends Fragment implements OnMapReadyCallback {

    private static String TAG = SearchRouteFragment.class.getName();

    private static int SOURCE_REQUEST_CODE = 99;
    private static int DESTINATION_REQUEST_CODE = 98;
    private static final int over = 0;
    private GoogleMap mMap;
    private View view;
    private TextView txtSource, txtDestination;
    private String source=null, destination=null;
    private SupportMapFragment mapFragment;
    public SearchRouteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view==null){
            view = inflater.inflate(R.layout.fragment_search_route, container, false);
        }

        txtSource = view.findViewById(R.id.txt_source);
        txtDestination = view.findViewById(R.id.txt_destination);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map_safe_route);
        mapFragment.getMapAsync(this);

        txtSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAutocompleteActivity(SOURCE_REQUEST_CODE);
            }
        });

        txtDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAutocompleteActivity(DESTINATION_REQUEST_CODE);
            }
        });

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        setUpGoogleMapScreen(googleMap);
        mMap = googleMap;
        DrawOnMap drawOnMap = new DrawOnMap();
        drawOnMap.draw(mMap);
        /*DirectionsResult result = directionsResult("483 George St, Sydney NSW 2000, Australia","182 Church St, Parramatta NSW 2150, Australia", TravelMode.DRIVING);
        if (result!=null) {
            Log.i(TAG, "Result: " + result.routes);
            drawPolyLine(result, googleMap);
            positionCamera(result.routes[over], googleMap);
            addMarker(result, googleMap);
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == SOURCE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                Log.i(TAG, "Source Selected: " + place);
                source = null;

                // Format the place's details and display them in the TextView.
                /*mPlaceDetailsText.setText(formatPlaceDetails(getResources(), place.getName(),
                        place.getId(), place.getAddress(), place.getPhoneNumber(),
                        place.getWebsiteUri()));*/
                //source = place.getName() + ", " + place.getAddress();
                source = place.getName().toString();
                Log.i(TAG, "Source: " + source);
                txtSource.setText(place.getName());
                checkSourceDestination();

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Log.e(TAG, "Error: Status = " + status.toString());
            } else if (resultCode == RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.
            }
        } else if (requestCode == DESTINATION_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                Log.i(TAG, "Destination Selected: " + place.getName());
                destination = null;

                //destination = place.getName() + ", " + place.getAddress();
                destination = place.getName().toString();
                Log.i(TAG, "Destination: " + destination);
                txtDestination.setText(place.getName());
                checkSourceDestination();

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Log.e(TAG, "Error: Status = " + status.toString());
            } else if (resultCode == RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.
            }
        }
    }

    private void checkSourceDestination() {
        Log.d(TAG, "Check source-destination: " + source + " " + destination);
        if (source!=null && destination!=null) {
            DirectionsResult result = directionsResult(source, destination, TravelMode.DRIVING);
            Log.d(TAG, "Direction result: " + result);
            if (result!=null) {
                mMap.clear();
                DrawOnMap drawOnMap = new DrawOnMap();
                drawOnMap.draw(mMap);
                drawPolyLine(result, mMap);
                positionCamera(result.routes[over], mMap);
                addMarker(result, mMap);
            }
        }
    }

    private void openAutocompleteActivity(int code) {
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            AutocompleteFilter filter = new AutocompleteFilter.Builder().setCountry("ID").build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(filter)
                    .build(getActivity());
            startActivityForResult(intent, code);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(getActivity(),
                    e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Log.e(TAG, message);
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }
    }

    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext
                .setQueryRateLimit(3)
                .setApiKey(getString(R.string.google_maps_key))
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
    }

    private void setUpGoogleMapScreen(GoogleMap map) {
        map.setBuildingsEnabled(true);
        map.setIndoorEnabled(true);
        //map.setTrafficEnabled(true);
        UiSettings uiSettings = map.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setCompassEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setScrollGesturesEnabled(true);
        uiSettings.setZoomGesturesEnabled(true);
        uiSettings.setTiltGesturesEnabled(true);
        uiSettings.setRotateGesturesEnabled(true);
    }

    private void addMarker(DirectionsResult result, GoogleMap map) {
        map.addMarker(new MarkerOptions().position(new LatLng(result
                .routes[over].legs[over].startLocation.lat,
                result.routes[over].legs[over].startLocation.lng))
                .title(result.routes[over].legs[over].startAddress));
        map.addMarker(new MarkerOptions().position(new LatLng(result
                .routes[over].legs[over].endLocation.lat,
                result.routes[over].legs[over].endLocation.lng))
                .title(result.routes[over].legs[over].startAddress).snippet(
                        getLocationTitle(result)));
    }

    private void positionCamera(DirectionsRoute route, GoogleMap map) {
        /*map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(route.legs[over]
                .endLocation.lat,
                route.legs[over].endLocation.lng), 13));*/
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(new LatLng(route.legs[over].startLocation.lat,
                route.legs[over].startLocation.lng));
        builder.include(new LatLng(route.legs[over].endLocation.lat,
                route.legs[over].endLocation.lng));
        LatLngBounds bound = builder.build();
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bound, 30));
    }

    private void drawPolyLine(DirectionsResult result, GoogleMap map) {
        List<LatLng> latLng = PolyUtil.decode(result
                .routes[over]
                .overviewPolyline
                .getEncodedPath());
        Log.d(TAG, "Routes coordinate" + latLng);
        map.addPolyline(new PolylineOptions().addAll(latLng));
    }

    private String getLocationTitle(DirectionsResult result) {
        return "Time: "+ result.routes[over].legs[over].duration.humanReadable +
                "Distance: " + result.routes[over].legs[over].distance.humanReadable;
    }

    private DirectionsResult directionsResult(String source, String destination, TravelMode mode) {
        DateTime now = new DateTime();
        try {
            return DirectionsApi.newRequest(getGeoContext())
                    .mode(mode)
                    .origin(source)
                    .destination(destination)
                    .departureTime(now)
                    .await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        }
    }

}
