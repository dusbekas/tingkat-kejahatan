package com.happy.group.bogorcrimelevel.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.happy.group.bogorcrimelevel.R;
import com.happy.group.bogorcrimelevel.adapter.SafeRouteAdapter;
import com.happy.group.bogorcrimelevel.model.Route;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShowRouteFragment extends Fragment {

    private String TAG = ShowRouteFragment.class.getName();

    private View view;
    private RecyclerView revSafeRouteList;
    private List<Route> routeList;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    public ShowRouteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view==null){
            view = inflater.inflate(R.layout.fragment_show_route, container, false);
        }

        routeList = new ArrayList<>();
        revSafeRouteList = view.findViewById(R.id.rev_route);
        revSafeRouteList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        revSafeRouteList.setLayoutManager(layoutManager);

        // Populate data
        //showData(getData(getContext()));
        getData(getContext());

        return view;
    }

    //private List<Route> getData(Context context) {
    private void getData(Context context) {
        List<Route> routes = new ArrayList<>();
        String s[] = context.getResources().getStringArray(R.array.predef_route);
        String r[] = context.getResources().getStringArray(R.array.predef_coordinate);
        for (int i = 0; i<s.length; i++) {
            Route route = new Route(s[i], r[i]);
            routes.add(route);

            Log.d(TAG, "Coordinate array " + i + ": " + r[i]);
        }

        //Log.d(TAG, "Routes: " + routes);
        //Log.d(TAG, "Routes size: " + routes.size());
        showData(routes);
        //return routes;
    }

    private void showData(List<Route> routes) {
        for (int i = 0; i<routes.size(); i++) {
            Route route = new Route();
            route.setName(routes.get(i).getName());
            route.setPoints(routes.get(i).getPoints());
            routeList.add(route);
            //Log.d(TAG, "Route as shown:" + routeList);
        }
        adapter = new SafeRouteAdapter(routeList, getContext());
        revSafeRouteList.setAdapter(adapter);
    }

}

