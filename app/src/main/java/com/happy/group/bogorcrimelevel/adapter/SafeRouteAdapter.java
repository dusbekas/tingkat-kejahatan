package com.happy.group.bogorcrimelevel.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.happy.group.bogorcrimelevel.R;
import com.happy.group.bogorcrimelevel.activity.SafeRouteActivity;
import com.happy.group.bogorcrimelevel.model.Route;

import java.util.List;

/**
 * Created by arman on 22/12/2017.
 */

public class SafeRouteAdapter extends RecyclerView.Adapter<SafeRouteAdapter.ViewHolder> {

    private List<Route> routes;
    private Context context;

    public SafeRouteAdapter(List<Route> routes, Context context) {
        this.routes = routes;
        this.context = context;
    }

    @Override
    public SafeRouteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_route, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Route route = routes.get(position);

        String name = route.getName();
        String points = route.getPoints();

        holder.name.setText(name);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SafeRouteActivity.class);
                intent.putExtra("POINTS", routes.get(position).getPoints());
                intent.putExtra("NAME", routes.get(position).getName());
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return routes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.txt_route);
        }
    }
}
