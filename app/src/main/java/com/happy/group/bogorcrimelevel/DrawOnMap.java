package com.happy.group.bogorcrimelevel;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.TravelMode;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by arman on 11/12/2017.
 */

public class DrawOnMap {

    private static final int COLOR_TRANSPARENT_ARGB = 0x00000000;
    private static final int COLOR_WHITE_ARGB = 0xffffffff;
    private static final int COLOR_RED_ARGB = 0x55FF2222;
    private static final int COLOR_ORANGE_ARGB = 0x55F57F17;
    private static final int COLOR_YELLOW_ARGB = 0x55F5F517;
    private static final int COLOR_GREEN_ARGB = 0x55388E3C;
    private static final int COLOR_BLUE_ARGB = 0xffF9A825;
    private static final int COLOR_PURPLE_ARGB = 0xff81C784;
    private static final int COLOR_BLACK_ARGB = 0xff000000;

    private static final int POLYGON_STROKE_WIDTH_PX = 5;
    private static final int CIRCLE_STROKE_WIDTH_PX = 3;
    private static final int PATTERN_DASH_LENGTH_PX = 20;
    private static final int PATTERN_GAP_LENGTH_PX = 20;
    private static final PatternItem DOT = new Dot();
    private static final PatternItem DASH = new Dash(PATTERN_DASH_LENGTH_PX);
    private static final PatternItem GAP = new Gap(PATTERN_GAP_LENGTH_PX);
    private static final int over = 0;

    // Create a stroke pattern of a gap followed by a dot.
    private static final List<PatternItem> PATTERN_POLYLINE_DOTTED = Arrays.asList(GAP, DOT);

    // Create a stroke pattern of a gap followed by a dash.
    private static final List<PatternItem> PATTERN_POLYGON_ALPHA = Arrays.asList(GAP, DASH);

    // Create a stroke pattern of a dot followed by a gap, a dash, and another gap.
    private static final List<PatternItem> PATTERN_POLYGON_BETA =
            Arrays.asList(DOT, GAP, DASH, GAP);

    public void draw(GoogleMap googleMap) {
        googleMap.clear();

        // Add polygons to indicate areas on the map.
        /*Polygon polygon1 = googleMap.addPolygon(new PolygonOptions()
                .clickable(true)
                .add(
                        new LatLng(-6.580354, 106.807127),
                        new LatLng(-6.591724, 106.818281),
                        new LatLng(-6.625197, 106.818631),
                        new LatLng(-6.622528, 106.789957),
                        new LatLng(-6.604023, 106.786629),
                        new LatLng(-6.584067, 106.788848)));
        // Store a data object with the polygon, used here to indicate an arbitrary type.
        //polygon1.setTag("alpha");
        stylePolygon(polygon1);*/

        // Position the map's camera near Bogor
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                -6.597525, 106.805932), 13));

        // 1=low, 2=med, 3=hi
        // radius in meter
        //drawCircle(googleMap, -6.596047, 106.804299, 1, 200);
        //drawCircle(googleMap, -6.586047, 106.794299, 1, 200);
        drawStackedCircle(googleMap, -6.586047, 106.794299, 3);
    }

    private void drawCircle(
            GoogleMap map, double latitude, double longitude, int level, int radius){
        int color=COLOR_TRANSPARENT_ARGB;
        switch (level){
            case 1:
                color=COLOR_GREEN_ARGB;
                break;
            case 2:
                color=COLOR_YELLOW_ARGB;
                break;
            case 3:
                color=COLOR_ORANGE_ARGB;
                break;
            case 4:
                color=COLOR_RED_ARGB;
                break;
        }
        map.addCircle(new CircleOptions()
                .center(new LatLng(latitude, longitude))
                .radius(radius)
                .strokeColor(color)
                .strokeWidth(CIRCLE_STROKE_WIDTH_PX)
                .fillColor(color));
    }

    private void drawStackedCircle(GoogleMap map, double latitude, double longitude, int level) {
        int radius = 100;
        for (int i=0; i<level; i++) {
            map.addCircle(new CircleOptions()
                    .center(new LatLng(latitude, longitude))
                    .radius(radius)
                    .strokeColor(COLOR_RED_ARGB)
                    .strokeWidth(CIRCLE_STROKE_WIDTH_PX)
                    .fillColor(COLOR_RED_ARGB));
            radius+=radius;
        }
    }

    /**
     * Styles the polygon, based on type.
     * @param polygon The polygon object that needs styling.
     */
    private void stylePolygon(Polygon polygon) {

        List<PatternItem> pattern = null;
        int strokeColor = COLOR_GREEN_ARGB;
        int fillColor = COLOR_TRANSPARENT_ARGB;

        polygon.setStrokePattern(pattern);
        polygon.setStrokeWidth(POLYGON_STROKE_WIDTH_PX);
        polygon.setStrokeColor(strokeColor);
        polygon.setFillColor(fillColor);
    }

    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext
                .setQueryRateLimit(3)
                .setApiKey(String.valueOf(R.string.google_maps_key))
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
    }

    public void setUpGoogleMapScreen(GoogleMap map) {
        map.setBuildingsEnabled(true);
        map.setIndoorEnabled(true);
        //map.setTrafficEnabled(true);
        UiSettings uiSettings = map.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setCompassEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setScrollGesturesEnabled(true);
        uiSettings.setZoomGesturesEnabled(true);
        uiSettings.setTiltGesturesEnabled(true);
        uiSettings.setRotateGesturesEnabled(true);
    }

    private void addMarker(DirectionsResult result, GoogleMap map) {
        map.addMarker(new MarkerOptions().position(new LatLng(result
                .routes[over].legs[over].startLocation.lat,
                result.routes[over].legs[over].startLocation.lng))
                .title(result.routes[over].legs[over].startAddress));
        map.addMarker(new MarkerOptions().position(new LatLng(result
                .routes[over].legs[over].endLocation.lat,
                result.routes[over].legs[over].endLocation.lng))
                .title(result.routes[over].legs[over].startAddress).snippet(
                        getLocationTitle(result)));
    }

    private void positionCamera(DirectionsRoute route, GoogleMap map) {
        /*map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(route.legs[over]
                .endLocation.lat,
                route.legs[over].endLocation.lng), 13));*/
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(new LatLng(route.legs[over].startLocation.lat,
                route.legs[over].startLocation.lng));
        builder.include(new LatLng(route.legs[over].endLocation.lat,
                route.legs[over].endLocation.lng));
        LatLngBounds bound = builder.build();
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bound, 30));
    }

    public void positionCamera(LatLng souce, LatLng destination, GoogleMap map) {
        /*map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(route.legs[over]
                .endLocation.lat,
                route.legs[over].endLocation.lng), 13));*/
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(souce);
        builder.include(destination);
        LatLngBounds bound = builder.build();
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bound, 50));
    }

    private void drawPolyLine(DirectionsResult result, GoogleMap map) {
        List<LatLng> latLng = PolyUtil.decode(result
                .routes[over]
                .overviewPolyline
                .getEncodedPath());
        map.addPolyline(new PolylineOptions().addAll(latLng));
    }

    private String getLocationTitle(DirectionsResult result) {
        return "Time: "+ result.routes[over].legs[over].duration.humanReadable +
                "Distance: " + result.routes[over].legs[over].distance.humanReadable;
    }

    private DirectionsResult directionsResult(String source, String destination, TravelMode mode) {
        DateTime now = new DateTime();
        try {
            return DirectionsApi.newRequest(getGeoContext())
                    .mode(mode)
                    .origin(source)
                    .destination(destination)
                    .departureTime(now)
                    .await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        }
    }

}
